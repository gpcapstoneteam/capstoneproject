#include "Animator.h"

#include <rttr/registration.h>

#include "AnimEngine.h"
#include "Resource/ResourceManager.h"
#include "Tools/Debug.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<Animator>("Animator")
		.constructor()(rttr::policy::ctor::as_raw_ptr)
		.method("Initialize", &Animator::Initialize)
		.method("Destroy", &Animator::Destroy)
		.method("Update", &Animator::Update)
		.method("GetSkeleton", &Animator::GetSkeleton)
		.method("GetJointTransform", &Animator::GetJointTransform)
		.method("AddClip", &Animator::CurrentClip)
		.method("BeginTransition", &Animator::BeginTransition)
		.method("IsTransitioning", &Animator::IsTransitioning)
		.method("CurrentTime", &Animator::CurrentTime)
		.method("CurrentClip", &Animator::CurrentClip);
}
#pragma warning( pop )

Animator::Animator() : isInitialized(false), globalTime(0.0f), model(nullptr), skeleton(nullptr), clipNames(), clips(), transitionHandler(nullptr), currentClip(0), currentPosNodes(), currentRotNodes(), currentScaleNodes(){
}

Animator::~Animator(){
	#ifdef _DEBUG
	if(skeleton != nullptr || !skeletonInstance.empty() || model != nullptr || 
		!clipNames.empty() || !clips.empty() || transitionHandler != nullptr ||
		!currentPosNodes.empty() || !currentRotNodes.empty() || !currentScaleNodes.empty()){
		Debug::LogWarning("Memory leak detected in Animator!", __FILE__, __LINE__);
		Destroy();
	}
	#endif //_DEBUG
}

bool Animator::Initialize(AnimModel* model_){
	_ASSERT(model_ != nullptr);
	_ASSERT(model_->skeleton != nullptr);

	model = model_;
	skeleton = model_->skeleton;
	skeletonInstance = skeleton->CreateInstance();

	for(unsigned int i = 0; i < skeleton->GetJointCount(); i++){
		currentPosNodes.push_back(nullptr);
		currentRotNodes.push_back(nullptr);
		currentScaleNodes.push_back(nullptr);
	}

	for(const auto& name : clipNames){
		AnimClip* temp = ResourceManager::LoadResource<AnimClip>(name);
		if(temp == nullptr){
			return false;
		}

		clips.push_back(temp);
	}

	AnimEngine::RegisterAnimator(this);

	isInitialized = true;
	return true;
}

void Animator::Destroy(){
	isInitialized = false;
	AnimEngine::UnregisterAnimator(this);

	currentPosNodes.clear();
	currentRotNodes.clear();
	currentScaleNodes.clear();

	for(auto& name : clipNames){
		ResourceManager::UnloadResource(name);
	}

	skeletonInstance.clear();
	skeletonInstance.shrink_to_fit();

	clipNames.clear();
	clipNames.shrink_to_fit();

	clips.clear();
	clips.shrink_to_fit();

	if(skeleton != nullptr){
		//We don't own the skeleton
		skeleton = nullptr;
	}

	if(model != nullptr){
		//We don't own the model
		model = nullptr;
	}

	if(transitionHandler != nullptr){
		delete transitionHandler;
		transitionHandler = nullptr;
	}
}

void Animator::Update(float deltaTime_){
	globalTime += deltaTime_;

	if(skeleton == nullptr || model == nullptr || clips.size() <= currentClip){
		return;
	}

	if(transitionHandler == nullptr){
		while(globalTime >= clips[currentClip]->GetLength()){
			globalTime -= clips[currentClip]->GetLength();
		}

		UpdateSkeletonInstance(clips[currentClip], globalTime);
	}else{
		float blendFactor = (globalTime - transitionHandler->StartTime()) / transitionHandler->Duration();

		UpdateSkeletonInstance(clips[GetClipID(transitionHandler->StartClip())], clips[GetClipID(transitionHandler->EndClip())], globalTime, globalTime - transitionHandler->StartTime(), blendFactor);

		if(blendFactor >= 1.0f){
			globalTime = transitionHandler->Duration();
			currentClip = GetClipID(transitionHandler->EndClip());

			for(int i = 0; i < currentPosNodes.size(); i++){
				currentPosNodes[i] = transitionHandler->currentPosNodes[i];
				currentRotNodes[i] = transitionHandler->currentRotNodes[i];
				currentScaleNodes[i] = transitionHandler->currentScaleNodes[i];
			}

			delete transitionHandler;
			transitionHandler = nullptr;
		}
	}
}

void Animator::UpdateSkeletonInstance(AnimClip* clip_, float time_){
	std::vector<Matrix4> globalTransforms;

	for(unsigned int i = 0; i < skeletonInstance.size(); i++){
		auto result = clip_->GetTransformAtTime(skeleton->GetJoint(i).name, time_, currentPosNodes[i], currentRotNodes[i], currentScaleNodes[i]);
		Matrix4 transform = result.result;

		currentPosNodes[i] = result.posNode;
		currentRotNodes[i] = result.rotNode;
		currentScaleNodes[i] = result.scaleNode;

		int parentID = skeleton->GetJoint(i).parentID;
		Matrix4 parentTransform = Matrix4::Identity();
		if(parentID >= 0){
			parentTransform = globalTransforms[parentID];
		}
		
		Matrix4 globalTransform = parentTransform * transform;
		globalTransforms.push_back(globalTransform);
		skeletonInstance[i] = model->globalInverse * globalTransform * skeleton->GetJoint(i).inverseBindPose;
	}
}

void Animator::UpdateSkeletonInstance(AnimClip* clipA_, AnimClip* clipB_, float timeA_, float timeB_, float blendFactor_){
	while(timeA_ > clipA_->GetLength()){
		timeA_ -= clipA_->GetLength();
	}

	while(timeB_ > clipB_->GetLength()){
		timeB_ -= clipB_->GetLength();
	}

	std::vector<PizzaBox::Matrix4> globalTransforms;

	for(unsigned int i = 0; i < skeletonInstance.size(); i++){
		PizzaBox::Matrix4 transform = GetBlendedTransform(skeleton->GetJoint(i).name, i, clipA_, clipB_, timeA_, timeB_, blendFactor_);

		int parentID = skeleton->GetJoint(i).parentID;
		PizzaBox::Matrix4 parentTransform = PizzaBox::Matrix4::Identity();
		if(parentID >= 0){
			parentTransform = globalTransforms[parentID];
		}

		PizzaBox::Matrix4 globalTransform = parentTransform * transform;
		globalTransforms.push_back(globalTransform);
		skeletonInstance[i] = model->globalInverse * globalTransform * skeleton->GetJoint(i).inverseBindPose;
	}
}

Matrix4 Animator::GetBlendedTransform(const std::string& joint_, unsigned int jointID_, AnimClip* clip1_, AnimClip* clip2_, float clip1Time_, float clip2Time_, float blendFactor){
	//Clip1 Translate
	auto result1 = clip1_->GetTranslateAtTime(joint_, clip1Time_, currentPosNodes[jointID_]);
	Vector3 clip1Translate = result1.result;
	currentPosNodes[jointID_] = result1.node;
	//Clip1 Rotate
	auto result2 = clip1_->GetRotateAtTime(joint_, clip1Time_, currentRotNodes[jointID_]);
	Quaternion clip1Rotate = result2.result;
	currentRotNodes[jointID_] = result2.node;
	//Clip1 Scale
	auto result3 = clip1_->GetScaleAtTime(joint_, clip1Time_, currentScaleNodes[jointID_]);
	Vector3 clip1Scale = result3.result;
	currentScaleNodes[jointID_] = result3.node;

	//Clip2 Translate
	auto result4 = clip2_->GetTranslateAtTime(joint_, clip2Time_, transitionHandler->currentPosNodes[jointID_]);
	Vector3 clip2Translate = result4.result;
	transitionHandler->currentPosNodes[jointID_] = result4.node;
	//Clip2 Rotate
	auto result5 = clip2_->GetRotateAtTime(joint_, clip2Time_, transitionHandler->currentRotNodes[jointID_]);
	Quaternion clip2Rotate = result5.result;
	transitionHandler->currentRotNodes[jointID_] = result5.node;
	//Clip2 Scale
	auto result6 = clip2_->GetScaleAtTime(joint_, clip2Time_, transitionHandler->currentScaleNodes[jointID_]);
	Vector3 clip2Scale = result6.result;
	transitionHandler->currentScaleNodes[jointID_] = result6.node;

	Vector3 finalTranslate = Vector3::Lerp(clip1Translate, clip2Translate, blendFactor);
	Quaternion finalRotate = Quaternion::Lerp(clip1Rotate, clip2Rotate, blendFactor);
	Vector3 finalScale = Vector3::Lerp(clip1Scale, clip2Scale, blendFactor);

	Matrix4 finalTransform = Matrix4::Identity();
	finalTransform *= Matrix4::Translate(finalTranslate);
	finalTransform *= finalRotate.ToMatrix4();
	finalTransform *= Matrix4::Scale(finalScale);

	return finalTransform;
}

void Animator::AddClip(const std::string& clipName_){
	clipNames.push_back(clipName_);
	if(isInitialized){
		AnimClip* temp = ResourceManager::LoadResource<AnimClip>(clipName_);
		if(temp == nullptr){
			Debug::LogError("Could not load AnimClip " + clipName_ + "!");
			throw std::exception("Could not load AnimClip");
		}

		clips.push_back(temp);
	}
}

void Animator::BeginTransition(const std::string& newClip_, float duration_){
	if(transitionHandler != nullptr){
		delete transitionHandler;
		transitionHandler = nullptr;
	}

	Transition t = Transition(GetClipName(currentClip), newClip_, duration_);
	transitionHandler = new TransitionHandler(t, globalTime, skeleton->GetJointCount());
}

unsigned int Animator::GetClipID(const std::string& clipName_) const{
	for(unsigned int i = 0; i < clipNames.size(); i++){
		if(clipNames[i] == clipName_){
			return i;
		}
	}

	Debug::LogError("Invalid Clip Name!", __FILE__, __LINE__);
	throw std::exception("Invalid Animation Clip!");
}

std::string Animator::GetClipName(unsigned int id_) const{
	_ASSERT(id_ < clipNames.size());
	return clipNames[id_];
}