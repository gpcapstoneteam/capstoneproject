#ifndef ANIM_CLIP_H
#define ANIM_CLIP_H

#include <map>
#include <vector>

#include "Math/Quaternion.h"
#include "Math/Vector.h"
#include "Resource/Resource.h"
#include "Tools/CDList.h"

namespace PizzaBox{
	template <class T>
	struct KeyFrame{
		KeyFrame(float time_, const T& value_) : time(time_), value(value_){}

		float time;
		T value;
	};

	template <class T1, class T2>
	struct ClipSearchResult{
		explicit ClipSearchResult(T2* node_ = nullptr) : result(), node(nullptr){
			if(node_ != nullptr){
				result = node_->value.value;
			}

			node = node_;
		}

		ClipSearchResult(T1 r_, T2* cur_) : result(r_), node(cur_){
		}

		T1 result;
		T2* node;
	};

	//Using declarations to simplify code that uses keyframes and search results
	using PosKeyFrame = KeyFrame<Vector3>;
	using RotKeyFrame = KeyFrame<Quaternion>;
	using ScaleKeyFrame = KeyFrame<Vector3>;

	using PosNode = CDList<PosKeyFrame>::Node;
	using RotNode = CDList<RotKeyFrame>::Node;
	using ScaleNode = CDList<ScaleKeyFrame>::Node;

	using PosResult = ClipSearchResult<Vector3, PosNode>;
	using RotResult = ClipSearchResult<Quaternion, RotNode>;
	using ScaleResult = ClipSearchResult<Vector3, ScaleNode>;

	struct FullClipSearchResult{
		FullClipSearchResult(const PosResult& pos_, const RotResult& rot_, const ScaleResult& scale_) : posNode(pos_.node), rotNode(rot_.node), scaleNode(scale_.node){
			result = Matrix4::Identity();
			result *= Matrix4::Translate(pos_.result);
			result *= rot_.result.ToMatrix4();
			result *= Matrix4::Scale(scale_.result);
		}

		Matrix4 result;
		PosNode* posNode;
		RotNode* rotNode;
		ScaleNode* scaleNode;
	};

	class AnimClip : public Resource{
	public:
		explicit AnimClip(const std::string& filePath_);
		virtual ~AnimClip() override;

		virtual bool Load() override;
		virtual void Unload() override;

		inline float GetLength() const{ return length; }
		inline void SetLength(float length_){ length = length_; }
		void AddPosKey(const std::string& name_, const PosKeyFrame& keyFrame_);
		void AddRotKey(const std::string& name_, const RotKeyFrame& keyFrame_);
		void AddScaleKey(const std::string& name_, const ScaleKeyFrame& keyFrame_);

		bool HasKeysForJoint(const std::string& name_) const;
		FullClipSearchResult GetTransformAtTime(const std::string& jointName_, float time_, PosNode* pNode_ = nullptr, RotNode* rNode_ = nullptr, ScaleNode* sNode_ = nullptr) const;

		PosResult GetTranslateAtTime(const std::string& jointName_, float time_, PosNode* node_) const;
		RotResult GetRotateAtTime(const std::string& jointName_, float time_, RotNode* node_) const;
		ScaleResult GetScaleAtTime(const std::string& jointName_, float time_, ScaleNode* node_) const;

	private:
		float length;
		std::map<std::string, CDList<PosKeyFrame>> posKeys;
		std::map<std::string, CDList<RotKeyFrame>> rotKeys;
		std::map<std::string, CDList<ScaleKeyFrame>> scaleKeys;

		CDList<PosKeyFrame>::Node* FindPositionKey(const std::string& jointName_, float time, PosNode* node_) const;
		CDList<RotKeyFrame>::Node* FindRotationKey(const std::string& jointName_, float time, RotNode* node_) const;
		CDList<ScaleKeyFrame>::Node* FindScaleKey(const std::string& jointName_, float time, ScaleNode* node_) const;

		Vector3 CalculateTranslation(const PosKeyFrame& prevKey_, const PosKeyFrame& nextKey_, float time_) const;
		Quaternion CalculateRotation(const RotKeyFrame& prevKey_, const RotKeyFrame& nextKey_, float time_) const;
		Vector3 CalculateScale(const ScaleKeyFrame& prevKey_, const ScaleKeyFrame& nextKey_, float time_) const;

		template <class T>
		typename CDList<T>::Node* ForwardSearch(const CDList<T>& keyFrames_, float time_, typename CDList<T>::Node* node_) const{
			auto curNode = node_;
			if(curNode == nullptr){
				curNode = keyFrames_.Front();
			}

			do{
				if(time_ >= curNode->value.time && time_ <= curNode->next->value.time){
					return curNode;
				}
				curNode = curNode->next;
			}while(curNode != nullptr && curNode != node_);

			return nullptr;
		}
	};
}

#endif //!ANIM_CLIP_H