#include "AnimClip.h"

#include <rttr/registration.h>

#include "Graphics/Models/ModelLoader.h"
#include "Math/Math.h"
#include "Tools/Debug.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<PosKeyFrame>("PosKeyFrame")
		.constructor<float, Vector3>()(rttr::policy::ctor::as_raw_ptr)
		.property("time", &PosKeyFrame::time)
		.property("value", &PosKeyFrame::value);

	rttr::registration::class_<RotKeyFrame>("RotKeyFrame")
		.constructor<float, Quaternion>()(rttr::policy::ctor::as_raw_ptr)
		.property("time", &RotKeyFrame::time)
		.property("value", &RotKeyFrame::value);

	rttr::registration::class_<ScaleKeyFrame>("ScaleKeyFrame")
		.constructor<float, Vector3>()(rttr::policy::ctor::as_raw_ptr)
		.property("time", &ScaleKeyFrame::time)
		.property("value", &ScaleKeyFrame::value);

	rttr::registration::class_<PosResult>("PosResult")
		//.constructor<PosNode>()(rttr::policy::ctor::as_raw_ptr) //These constructors cause template issues, TODO?
		//.constructor<Vector3, PosNode>()(rttr::policy::ctor::as_raw_ptr)
		.property("result", &PosResult::result)
		.property("node", &PosResult::node);

	rttr::registration::class_<RotResult>("RotResult")
		//.constructor<RotNode>()(rttr::policy::ctor::as_raw_ptr)
		//.constructor<Quaternion, RotNode>()(rttr::policy::ctor::as_raw_ptr)
		.property("result", &RotResult::result)
		.property("node", &RotResult::node);

	rttr::registration::class_<ScaleResult>("ScaleResult")
		//.constructor<ScaleNode>()(rttr::policy::ctor::as_raw_ptr)
		//.constructor<Vector3, ScaleNode>()(rttr::policy::ctor::as_raw_ptr)
		.property("result", &ScaleResult::result)
		.property("node", &ScaleResult::node);

	rttr::registration::class_<FullClipSearchResult>("FullClipSearchResult")
		.constructor<const PosResult&, const RotResult&, const ScaleResult&>()(rttr::policy::ctor::as_raw_ptr)
		.property("result", &FullClipSearchResult::result)
		.property("posNode", &FullClipSearchResult::posNode)
		.property("rotNode", &FullClipSearchResult::rotNode)
		.property("scaleNode", &FullClipSearchResult::scaleNode);

	rttr::registration::class_<AnimClip>("AnimClip")
		.constructor<std::string>()(rttr::policy::ctor::as_raw_ptr)
		.method("Load", &AnimClip::Load)
		.method("Unload", &AnimClip::Unload)
		.method("GetLength", &AnimClip::GetLength)
		.method("SetLength", &AnimClip::SetLength)
		.method("AddPosKey", &AnimClip::AddPosKey)
		.method("AddRotKey", &AnimClip::AddRotKey)
		.method("AddScaleKey", &AnimClip::AddScaleKey)
		.method("HasKeysForJoint", &AnimClip::HasKeysForJoint)
		.method("GetTransformAtTime", &AnimClip::GetTransformAtTime)
		.method("GetTranslateAtTime", &AnimClip::GetTranslateAtTime)
		.method("GetRotateAtTime", &AnimClip::GetRotateAtTime)
		.method("GetScaleAtTime", &AnimClip::GetScaleAtTime);
}
#pragma warning( pop )

AnimClip::AnimClip(const std::string& filePath_) : Resource(filePath_), length(0.0f), posKeys(), rotKeys(), scaleKeys(){
}

AnimClip::~AnimClip(){
	#ifdef _DEBUG
	if(!posKeys.empty() || !rotKeys.empty() || !scaleKeys.empty()){
		Debug::LogWarning("Memory leak detected in AnimClip!", __FILE__, __LINE__);
		Unload();
	}
	#endif //_DEBUG
}

bool AnimClip::Load(){
	if(ModelLoader::LoadAnimClips(fileName, *this) == false){
		Debug::LogError("Could not load animation clip " + fileName + "!");
		return false;
	}

	return true;
}

void AnimClip::Unload(){
	posKeys.clear();
	rotKeys.clear();
	scaleKeys.clear();
}

void AnimClip::AddPosKey(const std::string& jointName_, const PosKeyFrame& keyFrame_){
	if(posKeys.find(jointName_) == posKeys.end()){
		posKeys[jointName_] = CDList<PosKeyFrame>();
	}

	posKeys[jointName_].Add(keyFrame_);
}

void AnimClip::AddRotKey(const std::string& jointName_, const RotKeyFrame& keyFrame_){
	if(rotKeys.find(jointName_) == rotKeys.end()){
		rotKeys[jointName_] = CDList<RotKeyFrame>();
	}

	rotKeys[jointName_].Add(keyFrame_);
}

void AnimClip::AddScaleKey(const std::string& name_, const ScaleKeyFrame& keyFrame_){
	if(scaleKeys.find(name_) == scaleKeys.end()){
		scaleKeys[name_] = CDList<ScaleKeyFrame>();
	}

	scaleKeys[name_].Add(keyFrame_);
}

bool AnimClip::HasKeysForJoint(const std::string& name_) const{
	return	(posKeys.find(name_) != posKeys.end()) || 
			(rotKeys.find(name_) != rotKeys.end()) || 
			(scaleKeys.find(name_) != scaleKeys.end());
}

FullClipSearchResult AnimClip::GetTransformAtTime(const std::string& name_, float time_, PosNode* pNode_, RotNode* rNode_, ScaleNode* sNode_) const{
	return FullClipSearchResult(GetTranslateAtTime(name_, time_, pNode_),
		GetRotateAtTime(name_, time_, rNode_),
		GetScaleAtTime(name_, time_, sNode_));
}

CDList<PosKeyFrame>::Node* AnimClip::FindPositionKey(const std::string& jointName_, float time_, PosNode* node_) const{
	_ASSERT(posKeys.find(jointName_) != posKeys.end());
	return ForwardSearch<PosKeyFrame>(posKeys.at(jointName_), time_, node_);
}

CDList<RotKeyFrame>::Node* AnimClip::FindRotationKey(const std::string& jointName_, float time_, RotNode* node_) const{
	_ASSERT(rotKeys.find(jointName_) != rotKeys.end());
	return ForwardSearch<RotKeyFrame>(rotKeys.at(jointName_), time_, node_);
}

CDList<ScaleKeyFrame>::Node* AnimClip::FindScaleKey(const std::string& jointName_, float time_, ScaleNode* node_) const{
	_ASSERT(scaleKeys.find(jointName_) != scaleKeys.end());
	return ForwardSearch<ScaleKeyFrame>(scaleKeys.at(jointName_), time_, node_);
}

PosResult AnimClip::GetTranslateAtTime(const std::string& jointName_, float time_, PosNode* node_) const{
	if(posKeys.find(jointName_) == posKeys.end() || posKeys.at(jointName_).Size() == 0){
		return ClipSearchResult<Vector3, PosNode>(nullptr);
	}else if(posKeys.at(jointName_).Size() == 1 || time_ <= Math::NearZero() || time_ >= posKeys.at(jointName_).Back()->value.time){
		return ClipSearchResult<Vector3, PosNode>(posKeys.at(jointName_).Front());
	}

	CDList<PosKeyFrame>::Node* keyNode = FindPositionKey(jointName_, time_, node_);

	_ASSERT(keyNode != nullptr);

	//TODO - Do this if the animation is not supposed to loop
	//if(keyNode == posKeys.at(jointName_).Back()){
	//	return PosResult(keyNode);
	//}

	const PosKeyFrame& prevKey = keyNode->value;
	const PosKeyFrame& nextKey = keyNode->next->value;
	return PosResult(CalculateTranslation(prevKey, nextKey, time_), keyNode);
}

RotResult AnimClip::GetRotateAtTime(const std::string& name_, float time_, RotNode* node_) const{
	if(rotKeys.find(name_) == rotKeys.end() || rotKeys.at(name_).Size() == 0){
		return ClipSearchResult<Quaternion, RotNode>(nullptr);
	}else if(rotKeys.at(name_).Size() == 1 || time_ <= 0.0f || time_ >= rotKeys.at(name_).Back()->value.time){
		return ClipSearchResult<Quaternion, RotNode>(rotKeys.at(name_).Front());
	}

	CDList<RotKeyFrame>::Node* keyNode = FindRotationKey(name_, time_, node_);

	_ASSERT(keyNode != nullptr);

	//TODO - Do this if the animation is not supposed to loop
	//if(keyNode == rotKeys.at(name_).Back()){
	//	return RotResult(keyNode);
	//}

	const RotKeyFrame& prevKey = keyNode->value;
	const RotKeyFrame& nextKey = keyNode->next->value;
	return RotResult(CalculateRotation(prevKey, nextKey, time_), keyNode);
}

ScaleResult AnimClip::GetScaleAtTime(const std::string& name_, float time_, ScaleNode* node_) const{
	if(scaleKeys.find(name_) == scaleKeys.end() || scaleKeys.at(name_).Size() == 0){
		return ClipSearchResult<Vector3, ScaleNode>(nullptr);
	}else if(scaleKeys.at(name_).Size() == 1 || time_ <= 0.0f || time_ >= scaleKeys.at(name_).Back()->value.time){
		return ClipSearchResult<Vector3, ScaleNode>(scaleKeys.at(name_).Front());
	}

	auto keyNode = FindScaleKey(name_, time_, node_);

	_ASSERT(keyNode != nullptr);

	//TODO - Do this if the animation is not supposed to loop
	//if(keyNode == scaleKeys.at(name_).Back()){
	//	return ScaleResult(keyNode);
	//}

	const ScaleKeyFrame& prevKey = keyNode->value;
	const ScaleKeyFrame& nextKey = keyNode->next->value;
	return ScaleResult(CalculateScale(prevKey, nextKey, time_), keyNode);
}

Vector3 AnimClip::CalculateTranslation(const PosKeyFrame& prevKey_, const PosKeyFrame& nextKey_, float time_) const{
	float deltaTime = nextKey_.time - prevKey_.time;
	float factor = (time_ - prevKey_.time) / deltaTime;
	factor = Math::Clamp(0.0f, 1.0f, factor);

	return Vector3::Lerp(prevKey_.value, nextKey_.value, factor);
}

Quaternion AnimClip::CalculateRotation(const RotKeyFrame& prevKey_, const RotKeyFrame& nextKey_, float time_) const{
	float deltaTime = nextKey_.time - prevKey_.time;
	float factor = (time_ - prevKey_.time) / deltaTime;
	factor = Math::Clamp(0.0f, 1.0f, factor);

	return Quaternion::Lerp(prevKey_.value, nextKey_.value, factor);
}

Vector3 AnimClip::CalculateScale(const ScaleKeyFrame& prevKey_, const ScaleKeyFrame& nextKey_, float time_) const{
	float deltaTime = nextKey_.time - prevKey_.time;
	float factor = (time_ - prevKey_.time) / deltaTime;
	factor = Math::Clamp(0.0f, 1.0f, factor);

	return Vector3::Lerp(prevKey_.value, nextKey_.value, factor);
}