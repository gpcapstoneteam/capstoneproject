#ifndef TRANSITION_HANDLER_H
#define TRANSITION_HANDLER_H

#include "AnimClip.h"
#include "Transition.h"

namespace PizzaBox{
	class TransitionHandler{
	public:
		TransitionHandler(const Transition& transition_, float startTime_, unsigned int jointCount_) : transition(transition_), startTime(startTime_), currentPosNodes(), currentRotNodes(), currentScaleNodes(){
			for(unsigned int i = 0; i < jointCount_; i++){
				currentPosNodes.push_back(nullptr);
				currentRotNodes.push_back(nullptr);
				currentScaleNodes.push_back(nullptr);
			}
		}
		
		inline std::string StartClip() const{ return transition.GetStartClip(); }
		inline std::string EndClip() const{ return transition.GetEndClip(); }
		inline float Duration() const{ return transition.GetDuration(); }
		inline float StartTime() const{ return startTime; }

		std::vector<PosNode*> currentPosNodes;
		std::vector<RotNode*> currentRotNodes;
		std::vector<ScaleNode*> currentScaleNodes;

	private:
		Transition transition;
		float startTime;
	};
}

#endif //!TRANSITION_HANDLER_H