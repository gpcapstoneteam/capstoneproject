#ifndef RESOURCE_HANDLE_H
#define RESOURCE_HANDLE_H

#include "Resource.h"

namespace PizzaBox{
	template <class T>
	class ResourceHandle{
		friend class ResourceManager;

	public:
		ResourceHandle(){
			static_assert(std::is_base_of<Resource, T>::value, "T must inherit from Resource");
		}

		virtual void Drop() override{

		}

		T& operator->(){

		}
		const T& operator->() const{

		}

	private:
		std::string index;
	};
}

#endif //!RESOURCE_HANDLE_H