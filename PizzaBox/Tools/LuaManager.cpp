#include "LuaManager.h"

#include "Converter.h"
#include "Debug.h"

using namespace PizzaBox;

lua_State* LuaManager::state = nullptr;
std::vector<std::string> LuaManager::enabledScripts;
std::vector<rttr::variant> LuaManager::managedData;

bool LuaManager::Initialize(){
	#ifdef _DEBUG
	if(state != nullptr){
		Debug::LogError("Attempted to re-initialize LuaManager!", __FILE__, __LINE__);
		return false;
	}
	#endif //_DEBUG

	state = luaL_newstate();
	luaL_openlibs(state); //Allows use of the standard Lua libraries

	if(BindGlobalMethods() == false){
		Debug::LogError("Could not bind global methods to Lua!", __FILE__, __LINE__);
		return false;
	}

	if(BindClasses() == false){
		Debug::LogError("Could not bind classes to Lua!", __FILE__, __LINE__);
		return false;
	}

	if(Converter::RegisterConverterFunctions() == false){
		Debug::LogError("Could not register converter functions!", __FILE__, __LINE__);
		return false;
	}

	return true;
}

void LuaManager::Destroy(){
	CollectGarbage();
	managedData.shrink_to_fit();

	if(state != nullptr){
		lua_close(state);
		state = nullptr;
	}
}

bool LuaManager::EnableScript(const std::string& scriptName_, const std::string& script_){
	int result = luaL_dostring(state, script_.c_str());
	if(result != LUA_OK){
		Debug::LogError("Script was not executed successfully! Lua Error: " + std::string(lua_tostring(state, -1)));
		return false;
	}

	enabledScripts.push_back(scriptName_);
	return true;
}

void LuaManager::DisableScript(const std::string& scriptName_){
	if(state == nullptr || std::find(enabledScripts.begin(), enabledScripts.end(), scriptName_) != enabledScripts.end()){
		return;
	}

	//Set the script table to nil
	lua_checkstack(state, -1);
	lua_pushnil(state);
	lua_setglobal(state, scriptName_.c_str());

	enabledScripts.erase(std::remove(enabledScripts.begin(), enabledScripts.end(), scriptName_), enabledScripts.end());
}

bool LuaManager::ScriptHasFunction(const std::string& script_, const std::string& function_){
	if(std::find(enabledScripts.begin(), enabledScripts.end(), script_) == enabledScripts.end()){
		Debug::LogError("Cannot find functions on unloaded script [" + script_ + "]!", __FILE__, __LINE__);
		return false;
	}

	lua_getglobal(state, script_.c_str());
	if(!lua_istable(state, -1)){
		Debug::LogError("Could not find Lua table [" + script_ + "]!", __FILE__, __LINE__);
		return false;
	}

	lua_checkstack(state, 1);
	lua_pushstring(state, function_.c_str());
	lua_gettable(state, -2);

	if(!lua_isfunction(state, -1)){
		return false;
	}

	return true;
}

bool LuaManager::HasGlobalFunction(const std::string& function_){
	lua_getglobal(state, function_.c_str());
	if(!lua_isfunction(state, -1)){
		return false;
	}

	return true;
}

bool LuaManager::BindGlobalMethods(){
	//Create a table called 'Global'
	//Any global functions can be called through Lua scripts with the syntax:
	//Global.Function()

	if(rttr::type::get_global_methods().empty()){
		//There are no global functions, no need to do anything
		return true;
	}

	lua_checkstack(state, 1);
	lua_newtable(state);
	lua_pushvalue(state, -1);
	lua_setglobal(state, "Global");

	lua_pushvalue(state, -1);
	for(const auto& method : rttr::type::get_global_methods()){
		lua_checkstack(state, 3);
		//Push every registered global method into the Global table
		lua_pushstring(state, method.get_name().to_string().c_str());
		lua_pushlightuserdata(state, (void*)&method);
		lua_pushcclosure(state, CallGlobalFromLua, 1);
		lua_settable(state, -3);
	}

	return true;
}

bool LuaManager::BindClasses(){
	for(const auto& classToRegister : rttr::type::get_types()){
		if(!classToRegister.is_class() || (classToRegister.get_methods().empty() && classToRegister.get_constructors().empty())){
			//We're only going to bother registering classes
			//If the class has no methods or constructors then there's nothing to register
			continue;
		}

		std::string typeName = classToRegister.get_name().to_string();

		lua_checkstack(state, 1);
		lua_newtable(state);
		lua_pushvalue(state, -1);
		lua_setglobal(state, typeName.c_str());

		//Only bind [new] and [spawn] if this class has constructors
		if(!classToRegister.get_constructors().empty()){
			lua_checkstack(state, 3);
			lua_pushvalue(state, -1);
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, CreateUserData, 1);
			lua_setfield(state, -2, "new");

			lua_checkstack(state, 3);
			lua_pushvalue(state, -1);
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, SpawnUserData, 1);
			lua_setfield(state, -2, "spawn");
		}

		//Add static functions
		//Non-static functions will be found by the index metamethod
		for(const auto& method : classToRegister.get_methods()){
			if(method.is_static()){
				lua_checkstack(state, 4);
				lua_pushvalue(state, -1);
				lua_pushstring(state, typeName.c_str());
				lua_pushstring(state, method.get_name().to_string().c_str());
				lua_pushcclosure(state, InvokeStaticMethod, 2);
				lua_setfield(state, -2, method.get_name().to_string().c_str());
			}
		}

		luaL_newmetatable(state, MetaTableName(classToRegister).c_str());

		//The class only has a destructor available if it also has a constructor
		if(!classToRegister.get_constructors().empty()){
			lua_checkstack(state, 2);
			lua_pushstring(state, "__gc");
			lua_pushcfunction(state, DestroyUserData);
			lua_settable(state, -3);
		}

		lua_checkstack(state, 3);
		lua_pushstring(state, "__index");
		lua_pushstring(state, typeName.c_str());
		lua_pushcclosure(state, IndexUserData, 1);
		lua_settable(state, -3);

		lua_checkstack(state, 3);
		lua_pushstring(state, "__newindex");
		lua_pushstring(state, typeName.c_str());
		lua_pushcclosure(state, NewIndexUserData, 1);
		lua_settable(state, -3);

		if(classToRegister.get_method("__unm").is_valid()){
			lua_checkstack(state, 3);
			lua_pushstring(state, "__unm");
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, InvokeUNM, 1);
			lua_settable(state, -3);
		}

		if(classToRegister.get_method("__add").is_valid()){
			lua_checkstack(state, 3);
			lua_pushstring(state, "__add");
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, InvokeAdd, 1);
			lua_settable(state, -3);
		}

		if(classToRegister.get_method("__sub").is_valid()){
			lua_checkstack(state, 3);
			lua_pushstring(state, "__sub");
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, InvokeSub, 1);
			lua_settable(state, -3);
		}

		if(classToRegister.get_method("__mul").is_valid()){
			lua_checkstack(state, 3);
			lua_pushstring(state, "__mul");
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, InvokeMul, 1);
			lua_settable(state, -3);
		}

		if(classToRegister.get_method("__div").is_valid()){
			lua_checkstack(state, 3);
			lua_pushstring(state, "__div");
			lua_pushstring(state, typeName.c_str());
			lua_pushcclosure(state, InvokeDiv, 1);
			lua_settable(state, -3);
		}
	}

	return true;
}

std::string LuaManager::MetaTableName(const rttr::type& type_){
	if(type_.is_pointer()){
		return type_.get_raw_type().get_name().to_string() + "_MT_";
	}else if(type_.is_wrapper()){
		return type_.get_wrapped_type().get_raw_type().get_name().to_string() + "_MT_";
	}

	return (type_.get_name().to_string() + "_MT_");
}

void LuaManager::CollectGarbage(){
	int result = luaL_dostring(state, "collectgarbage()"); //Force full garbage collection
	if(result != LUA_OK){
		Debug::LogError("Could not invoke garbage collector! Lua Error: " + std::string(lua_tostring(state, -1)), __FILE__, __LINE__);
	}

	for(rttr::variant& v : managedData){
		v.get_type().destroy(v);
	}
	managedData.clear();
}

int LuaManager::InvokeMethod(const rttr::method& method_, const rttr::instance& object_){
	int luaStackOffset = 0;
	rttr::array_range<rttr::parameter_info> nativeParams = method_.get_parameter_infos();
	int numNativeArgs = static_cast<int>(nativeParams.size());
	int numLuaArgs = lua_gettop(state);

	int minNativeArgs = numNativeArgs;
	for(const auto& arg : nativeParams){
		if(arg.has_default_value()){
			minNativeArgs--;
		}
	}

	bool foundValidFunc = false;
	while(!foundValidFunc && numNativeArgs >= minNativeArgs){
		if(numLuaArgs < numNativeArgs || (object_.get_type().is_valid() && numLuaArgs <= numNativeArgs)){
			numNativeArgs--;
		}else{
			foundValidFunc = true;
		}
	}

	if(foundValidFunc && numLuaArgs > numNativeArgs){
		//If we're given too many arguments, just assume that the top of the stack has what we're looking for
		luaStackOffset = numLuaArgs - numNativeArgs;
		numLuaArgs = numNativeArgs;
	}else if(!foundValidFunc || numLuaArgs < numNativeArgs || (object_.get_type().is_valid() && numLuaArgs <= numNativeArgs)){
		//We can't properly call the function if we don't have enough arguments
		Debug::LogError("Invalid number of arguments passed to native function [" + method_.get_name().to_string() + "]! Expected " + std::to_string(minNativeArgs) + ", got " + std::to_string(numLuaArgs) + "!", __FILE__, __LINE__);
		return 0;
	}

	std::vector<rttr::argument> nativeArgs;
	nativeArgs.reserve(numNativeArgs);

	auto nativeParamsIt = nativeParams.begin();
	auto variantArgs = std::vector<rttr::variant>(numLuaArgs);

	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		const rttr::type& nativeParamType = nativeParamsIt->get_type();

		int luaArgIndex = i + 1 + luaStackOffset;
		int luaArgType = lua_type(state, luaArgIndex);

		nativeArgs.push_back(LuaToNativeArg(nativeParamType, luaArgIndex, luaArgType, i, variantArgs));
	}

	if(!CanInvokeMethod(method_, object_, true)){
		Debug::LogError("Method [" + method_.get_name().to_string() + "] cannot be invoked!", __FILE__, __LINE__);
		return 0;
	}

	rttr::variant result = method_.invoke_variadic(object_, nativeArgs);
	if(!result.is_valid()){
		Debug::LogError("Method [" + method_.get_name().to_string() + "] could not be invoked!", __FILE__, __LINE__);
		return 0;
	}

	return ToLuaValue(result);
}

bool LuaManager::CanInvokeMethod(const rttr::method& method_, const rttr::instance& object_, bool checkForErrors_){
	int luaStackOffset = 0;
	rttr::array_range<rttr::parameter_info> nativeParams = method_.get_parameter_infos();
	int numNativeArgs = static_cast<int>(nativeParams.size());
	int numLuaArgs = lua_gettop(state);

	int minNativeArgs = numNativeArgs;
	for(const auto& arg : nativeParams){
		if(arg.has_default_value()){
			minNativeArgs--;
		}
	}

	bool foundValidFunc = false;
	while(!foundValidFunc && numNativeArgs >= minNativeArgs){
		if(numLuaArgs < numNativeArgs || (object_.get_type().is_valid() && numLuaArgs <= numNativeArgs)){
			numNativeArgs--;
		}else{
			foundValidFunc = true;
		}
	}

	if(foundValidFunc && numLuaArgs > numNativeArgs){
		//If we're given too many arguments, just assume that the top of the stack has what we're looking for
		luaStackOffset = numLuaArgs - numNativeArgs;
		numLuaArgs = numNativeArgs;
	}else if(!foundValidFunc || numLuaArgs < numNativeArgs || (object_.get_type().is_valid() && numLuaArgs <= numNativeArgs)){
		//We can't properly call the function if we don't have enough arguments
		if(checkForErrors_){ Debug::LogError("Invalid number of arguments passed to native function [" + method_.get_name().to_string() + "]! Expected " + std::to_string(minNativeArgs) + ", got " + std::to_string(numLuaArgs) + "!", __FILE__, __LINE__); }
		return false;
	}

	std::vector<rttr::argument> nativeArgs;
	nativeArgs.reserve(numNativeArgs);

	auto nativeParamsIt = nativeParams.begin();
	auto variantArgs = std::vector<rttr::variant>(numLuaArgs);

	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		const rttr::type& nativeParamType = nativeParamsIt->get_type();

		int luaArgIndex = i + 1 + luaStackOffset;
		int luaArgType = lua_type(state, luaArgIndex);

		nativeArgs.push_back(LuaToNativeArg(nativeParamType, luaArgIndex, luaArgType, i, variantArgs, checkForErrors_));
	}

	nativeParamsIt = nativeParams.begin();
	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		if(nativeArgs[i].get_type() != nativeParamsIt->get_type()){
			return false;
		}
	}

	return true;
}

bool LuaManager::CanInvokeConstructor(const rttr::constructor& ctor_, bool checkForErrors_){
	int luaStackOffset = 0;
	rttr::array_range<rttr::parameter_info> nativeParams = ctor_.get_parameter_infos();
	int numNativeArgs = static_cast<int>(nativeParams.size());
	int numLuaArgs = lua_gettop(state);

	if(numLuaArgs > numNativeArgs){
		//If we're given too many arguments, just assume that the top of the stack has what we're looking for
		luaStackOffset = numLuaArgs - numNativeArgs;
		numLuaArgs = numNativeArgs;
	} else if(numLuaArgs < numNativeArgs){
		//We can't properly call the function if we don't have enough arguments
		if(checkForErrors_){ Debug::LogError("Invalid number of arguments passed to constructor for [" + ctor_.get_instantiated_type().get_name().to_string() + "]! Expected " + std::to_string(numNativeArgs) + ", got " + std::to_string(numLuaArgs) + "!", __FILE__, __LINE__); }
		return false;
	}

	std::vector<rttr::argument> nativeArgs;
	nativeArgs.reserve(numNativeArgs);

	auto nativeParamsIt = nativeParams.begin();
	auto variantArgs = std::vector<rttr::variant>(numLuaArgs);

	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		const rttr::type& nativeParamType = nativeParamsIt->get_type();

		int luaArgIndex = i + 1 + luaStackOffset;
		int luaArgType = lua_type(state, luaArgIndex);

		nativeArgs.push_back(LuaToNativeArg(nativeParamType, luaArgIndex, luaArgType, i, variantArgs, checkForErrors_));
	}

	nativeParamsIt = nativeParams.begin();
	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		if(nativeArgs[i].get_type() != nativeParamsIt->get_type()){
			return false;
		}
	}

	return true;
}

int LuaManager::CallGlobalFromLua(lua_State* L_){
	if(!lua_isuserdata(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected userdata upvalue!", __FILE__, __LINE__);
		return 0;
	}

	rttr::method* m = (rttr::method*)lua_touserdata(L_, lua_upvalueindex(1));
	if(m == nullptr || !m->is_valid()){
		Debug::LogError("Could not get rttr::method from Lua!", __FILE__, __LINE__);
		return 0;
	}

	rttr::method& methodToInvoke = (*m);
	rttr::instance object = {};

	return InvokeMethod(methodToInvoke, object);
}

rttr::argument LuaManager::LuaToNativeArg(const rttr::type& nativeType_, int luaStackArgIndex_, int luaArgType_, int i_, std::vector<rttr::variant>& argData_, bool checkForErrors_){
	switch(luaArgType_){
		case LUA_TNUMBER:
			if(!lua_isnumber(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected a number!", __FILE__, __LINE__); 
				return nullptr;
			}
			argData_[i_] = lua_tonumber(state, luaStackArgIndex_);
			break;
		case LUA_TBOOLEAN:
			if(!lua_isboolean(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected a boolean value!", __FILE__, __LINE__); 
				return nullptr;
			}
			argData_[i_] = lua_toboolean(state, luaStackArgIndex_);
			break;
		case LUA_TSTRING:
			if(!lua_isstring(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected a string value!", __FILE__, __LINE__); 
				return nullptr;
			}
			if(nativeType_ == rttr::type::get<std::string>()){
				argData_[i_] = std::string(lua_tostring(state, luaStackArgIndex_));
			}else{
				argData_[i_] = (const char*)lua_tostring(state, luaStackArgIndex_);
			}
			break;
		case LUA_TFUNCTION:
			if(!lua_isfunction(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected a function value!", __FILE__, __LINE__); 
				return nullptr;
			}
			if(checkForErrors_) Debug::LogError("Cannot use Lua functions as arguments for native functions!", __FILE__, __LINE__); 
			return nullptr;
		case LUA_TUSERDATA:
			if(!lua_isuserdata(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected a userdata value!", __FILE__, __LINE__); 
				return nullptr;
			}

			argData_[i_] = *(rttr::variant*)lua_touserdata(state, luaStackArgIndex_);
			break;
		case LUA_TLIGHTUSERDATA:
			if(!lua_islightuserdata(state, luaStackArgIndex_)){
				if(checkForErrors_) Debug::LogError("Expected LightUserData!", __FILE__, __LINE__); 
				return nullptr;
			}
			if(checkForErrors_) Debug::LogError("Cannot use LightUserData as arguments for native functions!", __FILE__, __LINE__);
			return nullptr;
		case LUA_TNIL:
			argData_[i_] = nullptr;
			break;
		case LUA_TNONE:
			if(checkForErrors_) Debug::LogError("Argument type does not exist!", __FILE__, __LINE__);
			return nullptr;
		default:
			if(checkForErrors_) Debug::LogError("Unknown Lua Type " + std::to_string(luaArgType_) + "!", __FILE__, __LINE__);
			return nullptr;
	}

	if(argData_[i_].can_convert(nativeType_) == false){
		if(checkForErrors_) Debug::LogError("Variable of type [" + argData_[i_].get_type().get_name().to_string() + "] cannot be converted to desired type [" + nativeType_.get_name().to_string() + "]!", __FILE__, __LINE__);
		return nullptr;
	}

	if(argData_[i_].convert(nativeType_) == false || !argData_[i_].is_valid()){
		if(checkForErrors_) Debug::LogError("Variable of type [" + argData_[i_].get_type().get_name().to_string() + "] could not be converted to desired type [" + nativeType_.get_name().to_string() + "]!", __FILE__, __LINE__);
		return nullptr;
	}

	return rttr::argument(argData_[i_]);
}

int LuaManager::ToLuaValue(rttr::variant& result_){
	if(!result_.is_valid()){
		Debug::LogError("Result was invalid!", __FILE__, __LINE__);
		return 0;
	}

	if(result_.is_type<void>()){
		return 0;
	}else if(result_.is_type<bool>()){
		lua_checkstack(state, 1);
		lua_pushboolean(state, result_.get_value<bool>());
	}else if(result_.can_convert<std::string>()){
		std::string val = result_.convert<std::string>();
		lua_checkstack(state, 1);
		lua_pushstring(state, val.c_str());
	}else if(result_.is_type<const char*>()){
		lua_checkstack(state, 1);
		lua_pushstring(state, result_.get_value<const char*>());
	}else if(result_.can_convert<double>()){
		result_.convert<double>();
		lua_checkstack(state, 1);
		lua_pushnumber(state, result_.get_value<double>());
	}else if(result_.is_type<nullptr_t>() || (result_.get_type().is_pointer() && result_.get_value<void*>() == nullptr)){
		lua_checkstack(state, 1);
		lua_pushnil(state);
	}else if(result_.get_type().is_class() || result_.get_type().is_pointer()){
		return CreateUserDataFromVariant(result_);
	}else{
		Debug::LogError("Unhandled return type [" + result_.get_type().get_name().to_string() + "]!");
		return 0;
	}

	return 1;
}

int LuaManager::CreateUserDataFromVariant(rttr::variant& v_){
	rttr::variant* ud = (rttr::variant*)lua_newuserdata(state, sizeof(rttr::variant));
	int userDataStackIndex = lua_gettop(state);
	new (ud) rttr::variant(v_);

	if(ud == nullptr || !ud->is_valid()){
		Debug::LogError("Userdata could not be created!", __FILE__, __LINE__);
		return 0;
	}

	luaL_getmetatable(state, MetaTableName(v_.get_type()).c_str());
	lua_setmetatable(state, userDataStackIndex);

	lua_newtable(state);
	lua_setuservalue(state, userDataStackIndex);

	return 1;
}

int LuaManager::AddToLuaStack(){
	return 0;
}

int LuaManager::CreateUserData(lua_State* L_){
	return NewUserData(L_, true);
}

int LuaManager::SpawnUserData(lua_State* L_){
	return NewUserData(L_, false);
}

int LuaManager::NewUserData(lua_State* L_, bool managed_){
	if(!lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected string upvalue!", __FILE__, __LINE__);
		return 0;
	}

	const char* typeName = (const char*)lua_tostring(L_, lua_upvalueindex(1));
	rttr::type typeToCreate = rttr::type::get_by_name(typeName);

	/*---------------------------------------------------------------------------------------------------------*/
	int numLuaArgs = lua_gettop(L_);

	const rttr::constructor* constructorToUse = nullptr;
	for(const auto& method : typeToCreate.get_constructors()){
		int numNativeArgs = static_cast<int>(method.get_parameter_infos().size());
		if(numNativeArgs == numLuaArgs && CanInvokeConstructor(method)){
			constructorToUse = &method;
			break;
		}
	}

	if(constructorToUse == nullptr){
		Debug::LogError("Could not find valid constructor for [" + typeToCreate.get_name().to_string() + "]!", __FILE__, __LINE__);
		return 0;
	}

	std::vector<rttr::argument> nativeArgs;
	nativeArgs.reserve(constructorToUse->get_parameter_infos().size());

	auto nativeParamsIt = constructorToUse->get_parameter_infos().begin();

	auto variantArgs = std::vector<rttr::variant>(numLuaArgs);

	for(int i = 0; i < numLuaArgs; i++, nativeParamsIt++){
		nativeArgs.push_back(LuaToNativeArg(nativeParamsIt->get_type(), i + 1, lua_type(L_, i + 1), i, variantArgs));
	}
	/*---------------------------------------------------------------------------------------------------------*/

	auto var = typeToCreate.create(nativeArgs);
	if(!var.is_valid()){
		Debug::LogError("Userdata could not be created!", __FILE__, __LINE__);
		return 0;
	}

	if(managed_){
		managedData.push_back(var);
	}

	return CreateUserDataFromVariant(var);
}

int LuaManager::DestroyUserData(lua_State* L_){
	if(!lua_isuserdata(L_, -1)){
		Debug::LogError("Expected userdata value!", __FILE__, __LINE__);
		return 0;
	}

	rttr::variant* ud = (rttr::variant*)lua_touserdata(L_, -1);
	if(ud == nullptr || !ud->is_valid()){
		Debug::LogWarning("Attempted to destroy null userdata!", __FILE__, __LINE__);
		return 0;
	}

	ud->~variant();
	return 0;
}

int LuaManager::InvokeFuncOnUserData(lua_State* L_){
	if(!lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected string upvalue!", __FILE__, __LINE__);
		return 0;
	}

	std::string methodName = (const char*)lua_tostring(L_, lua_upvalueindex(1));

	auto ud = (rttr::variant*)lua_touserdata(L_, 1);
	if(ud == nullptr || !ud->is_valid()){
		Debug::LogError("Could not retrieve userdata from Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	if(ud->get_type().is_wrapper()){
		ud = &ud->extract_wrapped_value();
	}

	std::vector<rttr::method> possibleMethods;
	for(const auto& method : ud->get_type().get_methods()){
		if(method.get_name().to_string() == methodName){
			rttr::array_range<rttr::parameter_info> nativeParams = method.get_parameter_infos();
			int numNativeArgs = static_cast<int>(nativeParams.size());
			int numLuaArgs = lua_gettop(L_);

			int minNativeArgs = numNativeArgs;
			for(const auto& arg : nativeParams){
				if(arg.has_default_value()){
					minNativeArgs--;
				}
			}

			if(numLuaArgs >= minNativeArgs + 1){
				possibleMethods.push_back(method);
			}
		}
	}

	rttr::instance object(*ud);

	for(const auto& method : possibleMethods){
		if(CanInvokeMethod(method, object)){
			return LuaManager::InvokeMethod(method, object);
		}
	}

	Debug::LogError("Could not find an overload of [" + ud->get_type().get_name().to_string() + "::" + methodName + "] with " + std::to_string(lua_gettop(L_) - 1) + " parameters!", __FILE__, __LINE__);
	return 0;
}

int LuaManager::IndexUserData(lua_State* L_){
	if(!lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected string upvalue!", __FILE__, __LINE__);
		return 0;
	}

	const char* typeName = (const char*)lua_tostring(L_, lua_upvalueindex(1));
	rttr::type typeInfo = rttr::type::get_by_name(typeName);
	if(!typeInfo.is_valid()){
		Debug::LogError("Unrecognized type [" + std::string(typeName) + "]!", __FILE__, __LINE__);
		return 0;
	}

	if(lua_isuserdata(L_, 1) == false){
		Debug::LogError("Userdata not found on Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	if(lua_isstring(L_, 2) == false){
		Debug::LogError("Name of native property/method found on Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	auto ud = (rttr::variant*)lua_touserdata(L_, 1);
	if(ud == nullptr || !ud->is_valid()){
		Debug::LogError("Could not retrieve userdata from Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	const char* fieldName = (const char*)lua_tostring(L_, 2);
	rttr::method m = typeInfo.get_method(fieldName);
	if(m.is_valid()){
		lua_checkstack(L_, 2);
		lua_pushstring(L_, m.get_name().to_string().c_str());
		lua_pushcclosure(L_, InvokeFuncOnUserData, 1);
		return 1;
	}

	rttr::property p = typeInfo.get_property(fieldName);
	if(p.is_valid()){
		auto ud = (rttr::variant*)lua_touserdata(L_, 1);
		rttr::variant result = p.get_value(*ud);
		if(result.is_valid()){
			return LuaManager::ToLuaValue(result);
		}
	}

	//If not a method or property, assume it's a uservalue
	lua_getuservalue(L_, 1);
	lua_checkstack(L_, 2);
	lua_pushvalue(L_, 2);
	lua_gettable(L_, -2);
	return 1; //This will return nil even if nothing is there
}

int LuaManager::NewIndexUserData(lua_State* L_){
	if(!lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected string upvalue!", __FILE__, __LINE__);
		return 0;
	}

	const char* typeName = (const char*)lua_tostring(L_, lua_upvalueindex(1));
	rttr::type typeInfo = rttr::type::get_by_name(typeName);
	if(!typeInfo.is_valid()){
		Debug::LogError("Unrecognized type [" + std::string(typeName) + "]!", __FILE__, __LINE__);
		return 0;
	}

	if(lua_isuserdata(L_, 1) == false){
		Debug::LogError("Userdata not found on Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	if(lua_isstring(L_, 2) == false){
		Debug::LogError("Could not retrieve userdata from Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	const char* fieldName = (const char*)lua_tostring(L_, 2);
	rttr::property p = typeInfo.get_property(fieldName);
	if(p.is_valid()){
		auto ud = (rttr::variant*)lua_touserdata(L_, 1);
		auto variantArgs = std::vector<rttr::variant>(1);
		p.set_value(*ud, LuaManager::LuaToNativeArg(p.get_type(), 3, lua_type(L_, 3), 0, variantArgs));

		return 0;
	}

	lua_getuservalue(L_, 1);
	lua_checkstack(L_, 2);
	lua_pushvalue(L_, 2);
	lua_pushvalue(L_, 3);
	lua_settable(L_, -3); //1[2] = 3

	return 0;
}

int LuaManager::InvokeStaticMethod(lua_State* L_){
	if(!lua_isstring(L_, lua_upvalueindex(1)) || !lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected two string upvalues!", __FILE__, __LINE__);
		return 0;
	}

	const char* typeName = (const char*)lua_tostring(L_, lua_upvalueindex(1));
	rttr::type typeInfo = rttr::type::get_by_name(typeName);
	if(!typeInfo.is_valid()){
		Debug::LogError("Unrecognized type [" + std::string(typeName) + "]!", __FILE__, __LINE__);
		return 0;
	}

	const char* staticMethodName = (const char*)lua_tostring(L_, lua_upvalueindex(2));
	rttr::method staticMethod = typeInfo.get_method(staticMethodName);
	if(!staticMethod.is_valid()){
		Debug::LogError("Could not find static method [" + std::string(typeName) + "::" + std::string(staticMethodName) + "]!", __FILE__, __LINE__);
	}

	rttr::instance obj = {};
	return LuaManager::InvokeMethod(staticMethod, obj);
}

int LuaManager::InvokeSpecialMethod(lua_State* L_, const std::string& methodName_){
	if(!lua_isstring(L_, lua_upvalueindex(1))){
		Debug::LogError("Expected string upvalue!", __FILE__, __LINE__);
		return 0;
	}

	std::string typeName = (const char*)lua_tostring(L_, lua_upvalueindex(1));
	rttr::type typeInfo = rttr::type::get_by_name(typeName);
	if(!typeInfo.is_valid()){
		Debug::LogError("Unrecognized type [" + typeName + "]!", __FILE__, __LINE__);
		return 0;
	}

	if(lua_isuserdata(L_, 1) == false){
		Debug::LogError("Userdata not found on Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	auto ud = (rttr::variant*)lua_touserdata(L_, 1);
	if(ud == nullptr || !ud->is_valid()){
		Debug::LogError("Could not retrieve userdata from Lua stack!", __FILE__, __LINE__);
		return 0;
	}

	rttr::method method = typeInfo.get_method(methodName_);
	if(!method.is_valid()){
		Debug::LogError("[" + typeInfo.get_name().to_string() + "] has no [" + methodName_ + "] method!", __FILE__, __LINE__);
		return 0;
	}

	return LuaManager::InvokeMethod(method, *ud);
}

int LuaManager::InvokeUNM(lua_State* L_){
	return InvokeSpecialMethod(L_, "__unm");
}

int LuaManager::InvokeAdd(lua_State* L_){
	return InvokeSpecialMethod(L_, "__add");
}

int LuaManager::InvokeSub(lua_State* L_){
	return InvokeSpecialMethod(L_, "__sub");
}

int LuaManager::InvokeMul(lua_State* L_){
	return InvokeSpecialMethod(L_, "__mul");
}

int LuaManager::InvokeDiv(lua_State* L_){
	return InvokeSpecialMethod(L_, "__div");
}