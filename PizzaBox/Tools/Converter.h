#ifndef CONVERTER_H
#define CONVERTER_H

namespace PizzaBox{
	class Converter{
	public:
		static bool RegisterConverterFunctions();

		template <class T>
		static T PointerToValue(T* value_, bool& ok_){
			if(value_ == nullptr){
				ok_ = false;
				throw std::exception("Null pointer!");
			}

			T val = *value_;
			ok_ = true;
			return val;
		}

		//Delete unwanted compiler generated constructors, destructors and assignment operators
		Converter() = delete;
		Converter(const Converter&) = delete;
		Converter(Converter&&) = delete;
		Converter& operator=(const Converter&) = delete;
		Converter& operator=(Converter&&) = delete;
		~Converter() = delete;
	};
}

#endif //!CONVERTER_H