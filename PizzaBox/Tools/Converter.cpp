#include "Converter.h"

#include <rttr/registration.h>

#include "Graphics/Color.h"
#include "Graphics/UI/ButtonUI.h"
#include "Graphics/UI/StatsTextUI.h"
#include "Math/Euler.h"
#include "Math/Rect.h"
#include "Math/Vector.h"

using namespace PizzaBox;

#define RegPTV(t) rttr::type::get<t>().register_converter_func(PointerToValue<t>)

bool Converter::RegisterConverterFunctions(){
	//Everything we pass to Lua is a pointer, which cannot be implicitly converted to their respective objects
	RegPTV(Color); RegPTV(ButtonUI); RegPTV(StatsTextUI); RegPTV(Euler); RegPTV(Rect); RegPTV(Vector3);

	return true;
}