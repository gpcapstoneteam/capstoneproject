#ifndef CD_LIST_H
#define CD_LIST_H

#include "Debug.h"

namespace PizzaBox{
	template <class T>
	class CDList{
	public:
		struct Node{
			Node(T value_) : value(value_), previous(nullptr), next(nullptr){
			}

			T value;
			Node* previous;
			Node* next;
		};

		CDList() : size(0), head(nullptr), tail(nullptr){
		}

		~CDList(){
			while(!IsEmpty()){
				Pop();
			}
		}

		void Add(T value_){
			_ASSERT(size < std::numeric_limits<size_t>::max());

			Node* tmp = new Node(value_);

			if(head == nullptr){
				_ASSERT(tail == nullptr);
				head = tmp;
				tail = tmp;

				head->next = tail;
				tail->previous = head;
			}else{
				tail->next = tmp;
				tmp->previous = tail;
				tail = tail->next;
			}

			//Keeps the list circular
			tail->next = head;
			head->previous = tail;

			size++;
		}

		void AddFront(T value_){
			_ASSERT(size < std::numeric_limits<size_t>::max());

			Node* tmp = new Node(value_);

			if(head == nullptr){
				_ASSERT(tail == nullptr);
				head = tmp;
				tail = tmp;
			}else{
				head->previous = tmp;
				tmp->next = head;
				head = head->previous;
			}

			//Keeps the list circular
			tail->next = head;
			head->previous = tail;

			size++;
		}

		void Pop(){
			if(IsEmpty()){
				return;
			}else if(head == tail){
				delete head;
				head = nullptr;
				tail = nullptr;
			}else{
				tail = tail->previous;
				delete tail->next;
				tail->next = nullptr;

				//Keeps the list circular
				tail->next = head;
				head->previous = tail;
			}

			size--;
		}

		void PopFront(){
			if(IsEmpty()){
				return;
			}else if(head == tail){
				delete head;
				head = nullptr;
				tail = nullptr;
			}else{
				head = head->next;
				delete head->previous;
				head->previous = nullptr;

				//Keeps the list circular
				tail->next = head;
				head->previous = tail;
			}

			size--;
		}

		size_t Size() const{
			return size;
		}

		bool IsEmpty() const{
			return (size == 0);
		}

		Node* Front() const{
			return head;
		}

		Node* Back() const{
			return tail;
		}

	private:
		size_t size;
		Node* head;
		Node* tail;
	};
}

#endif //!CD_LIST_H