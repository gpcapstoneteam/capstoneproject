#ifndef LUA_SCRIPT_H
#define LUA_SCRIPT_H

#include "LuaManager.h"
#include "Resource/Resource.h"

namespace PizzaBox{
	class LuaScript : public Resource{
	public:
		LuaScript(const std::string& filePath_);
		virtual ~LuaScript() override;

		virtual bool Load() override;
		virtual void Unload() override;

		bool HasFunction(const std::string& function_);

		template <typename... ARGS>
		void CallFunction(const std::string& function_, const ARGS& ... args_){
			_ASSERT(!scriptName.empty()); //Script must be loaded
			LuaManager::CallScriptFunction(scriptName, function_, args_...);
		}

	private:
		std::string scriptName;
	};
}

#endif //!LUA_SCRIPT_H