#include "DirectionalLight.h"

#include <rttr/registration.h>

#include "../Shader.h"
#include "../RenderEngine.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<DirectionalLight>("DirectionalLight")
		.constructor()(rttr::policy::ctor::as_raw_ptr)
		.constructor<float>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<float, Color>()(rttr::policy::ctor::as_raw_ptr)
		.method("Initialize", &DirectionalLight::Initialize)
		.method("Destroy", &DirectionalLight::Destroy)
		.method("GetEnable", &Component::GetEnable)
		.method("GetGameObject", &Component::GetGameObject)
		.method("SetEnable", &Component::SetEnable)
		.method("GetAmbient", &LightSource::GetAmbient)
		.method("GetDiffuse", &LightSource::GetDiffuse)
		.method("GetSpecular", &LightSource::GetSpecular)
		.method("GetIntensity", &LightSource::GetIntensity)
		.method("GetColor", &LightSource::GetColor)
		.method("CastsShadows", &LightSource::CastsShadows)
		.method("GetLightViewMatrix", &LightSource::GetLightViewMatrix)
		.method("GetLightSpaceMatrix", &LightSource::GetLightSpaceMatrix)
		.method("GetDepthMap", &LightSource::GetDepthMap)
		.method("SetAmbient", &LightSource::SetAmbient)
		.method("SetDiffuse", &LightSource::SetDiffuse)
		.method("SetSpecular", &LightSource::SetSpecular)
		.method("SetIntensity", &LightSource::SetIntensity)
		.method("SetColor", &LightSource::SetColor)
		.method("SetCastsShadows", &LightSource::SetCastsShadows)
		.method("SetLightViewMatrix", &LightSource::SetLightViewMatrix)
		.method("SetLightSpaceMatrix", &LightSource::SetLightSpaceMatrix)
		.method("SetDepthMap", &LightSource::SetDepthMap);
}
#pragma warning( pop )

DirectionalLight::DirectionalLight(float intensity_, Color lightColor_) : LightSource(intensity_, lightColor_){
	ambient = Color(0.05f, 0.05f, 0.05f);
	diffuse = Color(0.4f, 0.4f, 0.4f);
	specular = Color(0.5f, 0.5f, 0.5f);

}
 
DirectionalLight::~DirectionalLight(){
}

bool DirectionalLight::Initialize(GameObject* go_){
	//Makes sure the GameObject is a valid pointer
	_ASSERT(go_ != nullptr);
	gameObject = go_;
	 
	RenderEngine::RegisterDirectionalLight(this);
	return true;
}

void DirectionalLight::Destroy(){
	RenderEngine::UnregisterDirectionalLight(this);
	gameObject = nullptr;
}