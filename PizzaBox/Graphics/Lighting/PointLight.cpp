#include "PointLight.h"

#include <rttr/registration.h>

#include "../Shader.h"
#include "../RenderEngine.h"
#include "../../Math/Math.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<PointLight>("PointLight")
		.constructor()(rttr::policy::ctor::as_raw_ptr)
		.constructor<float>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<float, Color>()(rttr::policy::ctor::as_raw_ptr)
		.method("Initialize", &DirectionalLight::Initialize)
		.method("Destroy", &DirectionalLight::Destroy)
		.method("GetEnable", &Component::GetEnable)
		.method("GetGameObject", &Component::GetGameObject)
		.method("SetEnable", &Component::SetEnable)
		.method("GetAmbient", &LightSource::GetAmbient)
		.method("GetDiffuse", &LightSource::GetDiffuse)
		.method("GetSpecular", &LightSource::GetSpecular)
		.method("GetIntensity", &LightSource::GetIntensity)
		.method("GetColor", &LightSource::GetColor)
		.method("CastsShadows", &LightSource::CastsShadows)
		.method("GetLightViewMatrix", &LightSource::GetLightViewMatrix)
		.method("GetLightSpaceMatrix", &LightSource::GetLightSpaceMatrix)
		.method("GetDepthMap", &LightSource::GetDepthMap)
		.method("SetAmbient", &LightSource::SetAmbient)
		.method("SetDiffuse", &LightSource::SetDiffuse)
		.method("SetSpecular", &LightSource::SetSpecular)
		.method("SetIntensity", &LightSource::SetIntensity)
		.method("SetColor", &LightSource::SetColor)
		.method("SetCastsShadows", &LightSource::SetCastsShadows)
		.method("SetLightViewMatrix", &LightSource::SetLightViewMatrix)
		.method("SetLightSpaceMatrix", &LightSource::SetLightSpaceMatrix)
		.method("SetDepthMap", &LightSource::SetDepthMap)
		.method("GetConstant", &PointLight::GetConstant)
		.method("GetLinear", &PointLight::GetLinear)
		.method("GetQuadratic", &PointLight::GetQuadratic)
		.method("SetLinear", &PointLight::GetLinear)
		.method("SetQuadratic", &PointLight::GetQuadratic)
		.method("SetRange", &PointLight::SetRange);
}
#pragma warning( pop )

PointLight::PointLight(float intensity_, const Color& lightColor_, float range_) : LightSource(intensity_, lightColor_), linear(0.09f), quadratic(0.032f){
	ambient = Color(0.05f, 0.05f, 0.05f);
	diffuse = Color(0.8f, 0.8f, 0.8f);
	specular = Color(1.0f, 1.0f, 1.0f);
	
	SetRange(range_);
}

PointLight::~PointLight(){
}

bool PointLight::Initialize(GameObject* go_){
	//Makes sure the GameObject is a valid pointer
	_ASSERT(go_ != nullptr);
	gameObject = go_;

	RenderEngine::RegisterPointLight(this);
	return true;
}

void PointLight::Destroy(){
	RenderEngine::UnregisterPointLight(this);
	gameObject = nullptr;
}

void PointLight::SetRange(float range_){
	range_ *= 10.0f;

	linear = (constant / Math::Sqrt(range_)) / range_;
	quadratic = ((constant - linear) / Math::Sqrt(range_)) / range_;
}