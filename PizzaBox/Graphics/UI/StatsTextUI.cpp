#include "StatsTextUI.h"

#include <sstream>
#include <iomanip>

#include <rttr/registration.h>

#include "Tools/EngineStats.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<StatsTextUI>("StatsTextUI")
		.constructor<const std::string&, const Rect&, const std::string&>()(rttr::policy::ctor::as_raw_ptr)
		.property("name", &UIElement::name)
		.property("rect", &UIElement::rect)
		.method("Initialize", &UIElement::Initialize)
		.method("Destroy", &UIElement::Destroy)
		.method("Render", &UIElement::Render)
		.method("OnClick", &UIElement::OnClick)
		.method("OnRelease", &UIElement::OnRelease)
		.method("OnSelect", &UIElement::OnSelect)
		.method("OnDeselect", &UIElement::OnDeselect)
		.method("SetEnable", &UIElement::SetEnable)
		.method("GetEnable", &UIElement::GetEnable)
		.method("GetName", &UIElement::GetName)
		.method("GetRect", &UIElement::GetRect)
		.method("GetScript", &UIElement::GetScript)
		.method("GetFunction", &UIElement::GetFunction)
		.method("IsSelectable", &UIElement::IsSelectable)
		.method("ReceivesBackInput", &UIElement::ReceivesBackInput)
		.method("SetName", &UIElement::SetName)
		.method("SetRect", &UIElement::SetRect)
		.method("SetScript", &UIElement::SetScript)
		.method("SetFunction", &UIElement::SetFunction)
		.method("SetIsSelectable", &UIElement::SetIsSelectable)
		.method("GetText", &TextUI::GetText)
		.method("GetColor", &TextUI::GetColor)
		.method("SetText", &TextUI::SetText)
		.method("SetColor", &TextUI::SetColor);
}
#pragma warning( pop )


StatsTextUI::StatsTextUI(const std::string& name_, const Rect& rect_, const std::string& fontName_) : TextUI(name_, rect_, false, "FPS: 0.00", fontName_, nullptr){
}

StatsTextUI::~StatsTextUI(){
}

void StatsTextUI::Render(){
	std::stringstream fpsStream;
	fpsStream << std::fixed << std::setprecision(2) << EngineStats::GetFloat("Average FPS");

	std::stringstream aftStream;
	aftStream << std::fixed << std::setprecision(2) << EngineStats::GetFloat("Average Frame Time");

	SetText("FPS: " + fpsStream.str() + " (" + aftStream.str() + " ms)");
	TextUI::Render();
}