#include "Color.h"

#include <rttr/registration.h>

using namespace PizzaBox;

const Color Color::Red		= Color(1.0f, 0.0f, 0.0f, 1.0f);
const Color Color::Green	= Color(0.0f, 1.0f, 0.0f, 1.0f);
const Color Color::Blue		= Color(0.0f, 0.0f, 1.0f, 1.0f);
const Color Color::Yellow	= Color(1.0f, 1.0f, 0.0f, 1.0f);
const Color Color::Orange	= Color(1.0f, 0.5f, 0.0f, 1.0f);
const Color Color::Purple	= Color(0.4f, 0.0f, 0.8f, 1.0f);
const Color Color::Black	= Color(0.0f, 0.0f, 0.0f, 1.0f);
const Color Color::White	= Color(1.0f, 1.0f, 1.0f, 1.0f);
const Color Color::Brown	= Color(0.4f, 0.2f, 0.0f, 1.0f);
const Color Color::Gray		= Color(0.5f, 0.5f, 0.5f, 1.0f);
const Color Color::DarkGray = Color(0.2f, 0.2f, 0.2f, 1.0f);

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<Color>("Color")
		.constructor<float, float, float>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<float, float, float, float>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<const Vector3&>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<const Vector4&>()(rttr::policy::ctor::as_raw_ptr)
		.property("r", &Color::r)
		.property("g", &Color::g)
		.property("b", &Color::b)
		.property("a", &Color::a)
		.property_readonly("Red", &Color::Red)
		.property_readonly("Green", &Color::Green)
		.property_readonly("Blue", &Color::Blue)
		.property_readonly("Yellow", &Color::Yellow)
		.property_readonly("Orange", &Color::Orange)
		.property_readonly("Purple", &Color::Purple)
		.property_readonly("Black", &Color::Black)
		.property_readonly("White", &Color::White)
		.property_readonly("Brown", &Color::Brown)
		.property_readonly("Gray", &Color::Gray)
		.property_readonly("DarkGray", &Color::DarkGray);
}
#pragma warning( pop )

Color::Color(const float r_, const float g_, const float b_, const float a_) : r(r_), g(g_), b(b_), a(a_){
}

Color::Color(const Vector3& rgb) : r(rgb.x), g(rgb.y), b(rgb.z), a(1.0f){
}

Color::Color(const Vector4& rgba) :  r(rgba.x), g(rgba.y), b(rgba.z), a(rgba.w){
}

Color::~Color(){
}

Color::operator const float* () const{
	return static_cast<const float*>(&r);
}

Color::operator float* (){
	return static_cast<float*>(&r);
}