#include "ReflectiveMaterial.h"

#include <rttr/registration.h>

#include "Core/SceneManager.h"
#include "Graphics/RenderEngine.h"
#include "Graphics/Effects/Shadows.h"
#include "Resource/ResourceManager.h"

using namespace PizzaBox;

//Suppress meaningless and unavoidable warning
#pragma warning( push )
#pragma warning( disable : 26444 )
RTTR_REGISTRATION{
	rttr::registration::class_<ReflectiveMaterial>("ReflectiveMaterial")
		.constructor<bool>()(rttr::policy::ctor::as_raw_ptr)
		.constructor<bool, float>()(rttr::policy::ctor::as_raw_ptr)
		.property_readonly("air", &ReflectiveMaterial::air)
		.property_readonly("water", &ReflectiveMaterial::water)
		.property_readonly("ice", &ReflectiveMaterial::ice)
		.property_readonly("glass", &ReflectiveMaterial::glass)
		.property_readonly("diamond", &ReflectiveMaterial::diamond)
		.method("Initialize", &Component::Initialize)
		.method("Destroy", &Component::Destroy)
		.method("GetShader", &BaseMaterial::GetShader)
		.method("GetShaderName", &BaseMaterial::GetShaderName)
		.method("Update", &MeshMaterial::Update)
		.method("Render", &MeshMaterial::Render)
		.method("ReceivesShadows", &MeshMaterial::ReceivesShadows);
}
#pragma warning( pop )

ReflectiveMaterial::ReflectiveMaterial(bool isReflective_, float refractionIndex_) : MeshMaterial("ReflectiveShader"), isReflective(isReflective_), refractionIndex(refractionIndex_){
}

ReflectiveMaterial::~ReflectiveMaterial(){
}

bool ReflectiveMaterial::Initialize(){
	shader = ResourceManager::LoadResource<Shader>(shaderName);
	if(shader == nullptr){
		Debug::LogError(shaderName + " could not be loaded!", __FILE__, __LINE__);
		return false;
	}

	SetupUniforms();

	return true;
}

void ReflectiveMaterial::Destroy(){
	CleanupUniforms();

	if(shader != nullptr){
		ResourceManager::UnloadResource(shaderName);
		shader = nullptr;
	}
}

void ReflectiveMaterial::Render(const Camera* camera_, const Matrix4& model_, const std::vector<DirectionalLight*>& dirLights_, const std::vector<PointLight*>& pointLights_, const std::vector<SpotLight*>& spotLights) const{
	shader->Use();

	shader->BindMatrix4("projectionMatrix", camera_->GetProjectionMatrix());
	shader->BindMatrix4("viewMatrix", camera_->GetViewMatrix());
	shader->BindMatrix4("modelMatrix", model_);
	shader->BindVector3("viewPos", camera_->GetGameObject()->GlobalPosition());

	shader->BindInt("isReflective", isReflective);
	shader->BindFloat("refractionIndex", refractionIndex);
	shader->BindCubeMap(GL_TEXTURE0, SceneManager::CurrentScene()->GetSky()->GetSkyboxTexture());
	
	shader->BindColor("fogColor", RenderEngine::GetFogColor());
	shader->BindFloat("density", RenderEngine::GetFogDensity());
	shader->BindFloat("gradient", RenderEngine::GetFogGradient());
}